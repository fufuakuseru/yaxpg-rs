# Yet Another XKCD Passphrase Generator in Rust – yaxpg-rs

Generate [xkcd](https://xkcd.com/936/)-style passphrase like [xkpasswd](https://xkpasswd.net/s/) but locally using Rust.

## TODO

- [x] passphrase adt
- [x] generate passphrase
    - [x] generate affix
    - [x] generate separator
- [x] read wordlist file
- [x] launch arguments
- [x] config file
- [x] filter word based on length
- [x] generate specific length passphrase
    - [x] if passphrase of specific length cannot be generated change param


## Note

- wordlist_english was obtained from the wamerican package of which I stripped the lines containing `'s`
- wordlist_english was obtained from the wfrench package of which I stripped the lines containing `'` or `-`
- wordlist_latin was obtained from [this search](https://petscan.wmflabs.org/?language=la&project=wiktionary&categories=Lingua%20Latina&ns%5B0%5D=1&sortby=title&interface_language=en&active_tab=tab_output&&doit=) with a plain text format from which I stripped the lines containing `'`

## Running clippy with all the warnings

```
$ cargo clippy -- -W clippy::pedantic -W clippy::nursery -W clippy::unwrap_used -W clippy::expect_used
```