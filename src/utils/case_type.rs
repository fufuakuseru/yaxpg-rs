use clap::ValueEnum;
use rand::{distributions::Standard, prelude::Distribution};
use serde_derive::{Deserialize, Serialize};
use std::fmt;

/// `Enum` of possible cases used on words in the Passphrase
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Deserialize, Serialize)]
pub enum CaseType {
    Lower,
    Upper,
    Alternating,
    Random,
    Capitalized,
    AlternatingWords,
}

impl fmt::Display for CaseType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Lower => "lowercase",
                Self::Upper => "uppercase",
                Self::Alternating => "aLtErNaTiNg CaSe iN WoRd",
                Self::Random => "rANdoM in WOrD",
                Self::Capitalized => "Captitalized Words",
                Self::AlternatingWords => "alternating CASE between WORDS",
            },
        )
    }
}

impl Distribution<CaseType> for Standard {
    fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> CaseType {
        match rng.gen_range(0..=4) {
            0 => CaseType::Lower,
            1 => CaseType::Upper,
            2 => CaseType::Alternating,
            4 => CaseType::Capitalized,
            _ => CaseType::Random,
        }
    }
}

/// Return a `String` with a alternating case.
pub fn to_alternating_case(word: &str, is_start_upper: bool) -> String {
    let mut is_upper = is_start_upper;
    let mut new = String::new();
    for letter in word.chars() {
        if is_upper {
            new.push_str(&(letter.to_uppercase().to_string()));
            is_upper = false;
        } else {
            new.push_str(&(letter.to_lowercase().to_string()));
            is_upper = true;
        }
    }
    new
}

/// Return a `String` with a random case.
pub fn to_random_case(word: &str) -> String {
    let mut new = String::new();
    for letter in word.chars() {
        if rand::random() {
            new.push_str(&letter.to_uppercase().to_string());
        } else {
            new.push_str(&letter.to_lowercase().to_string());
        }
    }
    new
}

/// Return a `String` with a capitalized case.
pub fn to_capitalized_case(word: &str) -> String {
    word[0..1].to_uppercase() + &word[1..].to_lowercase()
}

impl CaseType {
    /// Converts a given `word` to a given `CaseType`
    pub fn convert(&self, word: &str) -> String {
        match self {
            Self::Lower => word.to_lowercase(),
            Self::Upper => word.to_uppercase(),
            Self::Random => to_random_case(word),
            Self::Alternating => to_alternating_case(word, rand::random()),
            _ => to_capitalized_case(word),
        }
    }
}
