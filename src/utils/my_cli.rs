use crate::utils::case_type::CaseType;
use clap::Parser;
use serde_derive::{Deserialize, Serialize};

/// CLI-arguments datatype
#[derive(Parser, Debug, Deserialize, Serialize, Default, Clone)]
#[command(author, version, about)]
pub struct MyCLI {
    /// path to wordlist file
    pub wordlist_path: Option<String>,
    /// number of words generated per passphrase
    #[arg(short, long)]
    pub nb_words: Option<u32>,
    /// minimum length of word used
    #[arg(short, long)]
    pub min_word_length: Option<u32>,
    /// maximum length of word used
    #[arg(short = 'M', long)]
    pub max_word_length: Option<u32>,
    /// number of passphrase generated
    #[arg(short = 'N', long)]
    pub nb_pass: Option<u32>,
    /// amount of digits generated in affixes
    #[arg(short, long)]
    pub digits_len: Option<u32>,
    /// amount of symbols generated in affixes
    #[arg(short, long)]
    pub symbols_len: Option<u32>,
    /// amount of symbols generated in separators
    #[arg(short = 'S', long)]
    pub separator_len: Option<u32>,
    /// case type used to transform words in passphrase
    #[arg(short, long, value_enum)]
    pub case: Option<CaseType>,
    /// path to read config file
    #[arg(short, long, value_name = "CONFIG_PATH")]
    pub read_config: Option<String>,
    /// path to save config file
    #[arg(long, value_name = "CONFIG_PATH")]
    pub save_config: Option<String>,
    /// path to output file
    #[arg(short, long, value_name = "OUTPUT")]
    pub output: Option<String>,
    /// string used to generate the digits of affixes
    #[arg(long)]
    pub digits_choice: Option<String>,
    /// string used to generate the symbols of affixes
    #[arg(long)]
    pub symbols_choice: Option<String>,
    /// set a specific prefix
    #[arg(long)]
    pub prefix: Option<String>,
    /// set a specific affix
    #[arg(long)]
    pub suffix: Option<String>,
    /// set a specific separator
    #[arg(long)]
    pub separator: Option<String>,
    #[arg(long)]
    /// minimum length of passphrases generated
    pub min_pass_length: Option<u32>,
    #[arg(long)]
    /// maximum length of passphrases generated
    pub max_pass_length: Option<u32>,
}

impl MyCLI {
    /// Returns a `MyCLI` updated by the values in `other_cli`
    pub fn from(mut self, other_cli: Self) -> Self {
        self.wordlist_path = other_cli.wordlist_path.or(self.wordlist_path);
        self.nb_words = self.nb_words.or(other_cli.nb_words);
        self.min_word_length = self.min_word_length.or(other_cli.min_word_length);
        self.max_word_length = self.max_word_length.or(other_cli.max_word_length);
        self.nb_pass = self.nb_pass.or(other_cli.nb_pass);
        self.digits_len = self.digits_len.or(other_cli.digits_len);
        self.symbols_len = self.symbols_len.or(other_cli.symbols_len);
        self.case = self.case.or(other_cli.case);
        self.read_config = self.read_config.or(other_cli.read_config);
        self.save_config = self.save_config.or(other_cli.save_config);
        self.output = self.output.or(other_cli.output);
        self.digits_choice = self.digits_choice.or(other_cli.digits_choice);
        self.symbols_choice = self.symbols_choice.or(other_cli.symbols_choice);
        self.prefix = self.prefix.or(other_cli.prefix);
        self.suffix = self.suffix.or(other_cli.suffix);
        self.separator = self.separator.or(other_cli.separator);
        self.min_pass_length = self.min_pass_length.or(other_cli.min_pass_length);
        self.max_pass_length = self.max_pass_length.or(other_cli.max_pass_length);
        self
    }
}
