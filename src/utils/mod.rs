pub mod case_type;
pub mod my_cli;
pub mod passphrase_settings;
pub mod program_param;

/// Adds a given `message` (formatted to be a given `width`) with a `value` to a `string`.
fn put_in_string(string: &mut String, message: &str, value: &str, width: usize) {
    string.push_str(&format!("\t{message:width$}: {value}\n"));
}
