use std::fmt;

use crate::utils::{my_cli::MyCLI, passphrase_settings::PassphraseSettings, put_in_string};

/// Parameters of the program
#[derive(Debug)]
pub struct ProgramParam<'a> {
    /// Cli used in the program
    pub cli: &'a MyCLI,
    /// PassphreaseSettings used in passphrase generation
    pub settings: &'a PassphraseSettings,
}

impl fmt::Display for ProgramParam<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut string = String::new();
        let width = 39;
        if let Some(path) = &self.cli.wordlist_path {
            put_in_string(&mut string, "Path of the wordlist file", path, width);
        }
        if let Some(path) = &self.cli.read_config {
            put_in_string(&mut string, "Path to the config file read", path, width);
        };
        if let Some(path) = &self.cli.save_config {
            put_in_string(&mut string, "Path to the config savefile", path, width);
        };
        if let Some(path) = &self.cli.output {
            put_in_string(&mut string, "Path to the output file", path, width);
        };
        string.push_str(&self.settings.to_string());
        write!(f, "{string}")
    }
}
