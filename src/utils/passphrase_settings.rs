use crate::utils::{case_type::CaseType, my_cli::MyCLI, put_in_string};
use std::{fmt, process};

/// Returns a `Vec<String>` using the file in `wordlist_path`.
/// If there is an error reading the file, the program exits.
/// If the file provided is empty, the program exits.
fn read_wordlist_file(wordlist_path: &str) -> Vec<String> {
    let contents = match std::fs::read_to_string(wordlist_path) {
        Ok(worldlist_found) => worldlist_found,
        Err(error_msg) => {
            eprintln!("Error reading wordlist file ({wordlist_path}) : {error_msg}");
            process::exit(1);
        }
    };
    let mut wl: Vec<String> = vec![];
    for each in contents.split_whitespace() {
        wl.push(each.to_string());
    }
    if wl.is_empty() {
        eprintln!("Wordlist file ({wordlist_path}) is empty.");
        process::exit(3);
    }
    wl.sort();
    wl.dedup();
    wl
}

/// Settings for generating `Passphrase` datatype
#[derive(Debug)]
pub struct PassphraseSettings {
    /// wordlist used to select words for phrase
    pub wordlist: Vec<String>,
    /// minimum length of words selected
    pub min_word_length: u32,
    /// maximum length of words selected
    pub max_word_length: u32,
    /// number of words used in phrase
    pub nb_words: u32,
    /// number of digits used in affix generation
    pub nb_digits: u32,
    /// number of symbols used in affix generation
    pub nb_symbols: u32,
    /// number of symbols used in separator generation
    pub nb_separator: u32,
    /// characters possible to generate the digits in affix generation
    pub digits_choice: Vec<char>,
    /// characters possible to generate the symbols in affix/separator generation
    pub symbols_choice: Vec<char>,
    /// set prefix
    pub prefix: Option<String>,
    /// set suffix
    pub suffix: Option<String>,
    /// set separator
    pub separator: Option<String>,
    /// set the case of the words in phrase
    pub case: Option<CaseType>,
    /// minimum length of passphrase(s) generated
    pub min_pass_length: u32,
    /// maximum length of passphrase(s) generated
    pub max_pass_length: u32,
    /// length of the smallest word in the wordlist
    pub wordlist_smallest_length: u32,
    /// length of the largest word in the wordlist
    pub wordlist_largest_length: u32,
}

impl fmt::Display for PassphraseSettings {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let width = 39;
        let mut string = String::new();
        put_in_string(
            &mut string,
            "Minimum length of words used",
            &self.min_word_length.to_string(),
            width,
        );
        put_in_string(
            &mut string,
            "Maximum length of words used",
            &self.max_word_length.to_string(),
            width,
        );
        put_in_string(
            &mut string,
            "Number of words used",
            &self.nb_words.to_string(),
            width,
        );
        {
            let print_affix_params = {
                put_in_string(
                    &mut string,
                    "Number of digits used in affix",
                    &self.nb_digits.to_string(),
                    width,
                );
                put_in_string(
                    &mut string,
                    "Number of symbols used in affix",
                    &self.nb_symbols.to_string(),
                    width,
                );
                put_in_string(
                    &mut string,
                    "Digits used in affix",
                    &self.digits_choice.iter().copied().collect::<String>(),
                    width,
                );
                put_in_string(
                    &mut string,
                    "Symbols used in affix",
                    &self.symbols_choice.iter().copied().collect::<String>(),
                    width,
                );
                0
            };
            self.prefix.as_ref().map_or_else(
                || {
                    let _: i32 = print_affix_params;
                },
                |pre| put_in_string(&mut string, "Prefix set", &pre.to_string(), width),
            );
            self.suffix.as_ref().map_or_else(
                || {
                    let _: i32 = print_affix_params;
                },
                |suf| put_in_string(&mut string, "Suffix set", &suf.to_string(), width),
            );
        }
        if let Some(sep) = &self.separator {
            put_in_string(&mut string, "Separator set", &sep.to_string(), width);
        } else {
            put_in_string(
                &mut string,
                "Number of symbols used in separator",
                &self.nb_symbols.to_string(),
                width,
            );
            put_in_string(
                &mut string,
                "Symbols used in separator",
                &self.symbols_choice.iter().copied().collect::<String>(),
                width,
            );
        };
        put_in_string(
            &mut string,
            "Minimum length of passphrase generated",
            &self.min_pass_length.to_string(),
            width,
        );
        put_in_string(
            &mut string,
            "Maximum length of passphrase generated",
            &self.max_pass_length.to_string(),
            width,
        );
        if let Some(case) = &self.case {
            put_in_string(&mut string, "Case type", &case.to_string(), width);
        }
        write!(f, "{string}")
    }
}

impl PassphraseSettings {
    /// Return a `Passphrase` with the given CLI-arguments
    pub fn new(cli: &MyCLI) -> Self {
        let min: u32 = cli.min_word_length.unwrap_or(3);
        let max: u32 = cli.max_word_length.unwrap_or(6);
        let wordlist = read_wordlist_file(&cli.wordlist_path.clone().unwrap_or_default());
        let nb_words = cli.nb_words.unwrap_or(4);
        let nb_digits = cli.digits_len.unwrap_or(2);
        let nb_symbols = cli.symbols_len.unwrap_or(2);
        let digits_choice: Vec<char> = cli
            .digits_choice
            .as_deref()
            .unwrap_or("0123456789")
            .chars()
            .collect();
        let symbols_choice: Vec<char> = cli
            .symbols_choice
            .as_deref()
            .unwrap_or("!@$^&*-_+|~?/.;")
            .chars()
            .collect();
        let nb_separator = cli.separator_len.unwrap_or(1);
        let prefix = cli.prefix.clone();
        let suffix = cli.suffix.clone();
        let separator = cli.separator.clone();
        let case = cli.case.clone();
        let min_pass_length = cli.min_pass_length.unwrap_or(1);
        let max_pass_length = cli.max_pass_length.unwrap_or(64);
        let mut wordlist_min: u32 = u32::MAX;
        let mut wordlist_max: u32 = u32::MIN;
        for each in &wordlist {
            let current_len = each.len().try_into().unwrap_or(max);
            if current_len < wordlist_min {
                wordlist_min = current_len;
            }
            if current_len > wordlist_max {
                wordlist_max = current_len;
            }
        }
        Self {
            wordlist,
            min_word_length: min,
            max_word_length: max,
            nb_words,
            nb_digits,
            nb_symbols,
            nb_separator,
            digits_choice,
            symbols_choice,
            prefix,
            suffix,
            separator,
            case,
            min_pass_length,
            max_pass_length,
            wordlist_smallest_length: wordlist_min,
            wordlist_largest_length: wordlist_max,
        }
    }
}
