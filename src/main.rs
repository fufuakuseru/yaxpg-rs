use clap::Parser;
use rand::prelude::SliceRandom;
use std::{fmt, process};

mod utils;
use crate::utils::{
    case_type::CaseType, my_cli::MyCLI, passphrase_settings::PassphraseSettings,
    program_param::ProgramParam,
};

/// `Enum` to tell if an affix is a prefix or a suffix
#[derive(Debug)]
enum AffixType {
    Prefix,
    Suffix,
}

/// Returns a `String` using the parameters provided
/// # Example
/// ```
/// let nb_digits = 1;
/// let nb_symbols = 1;
/// let digits_choice = vec!['1'];
/// let symbols_choice = vec!['-'];
/// assert_eq!(generate_affix(nb_digits, nb_symbols, &digits_choice, &symbols_choice, &AffixType::Prefix), "1-");
/// assert_eq!(generate_affix(nb_digits, nb_symbols, &digits_choice, &symbols_choice, &AffixType::Suffix), "-1");
/// ```
fn generate_affix(
    nb_digits: u32,
    nb_symbols: u32,
    digits_choice: &Vec<char>,
    symbols_choice: &Vec<char>,
    affix: &AffixType,
) -> String {
    let rng_0 = &mut rand::thread_rng();
    let mut selected_digits = String::new();
    for _ in 0..nb_digits {
        selected_digits.push(*digits_choice.choose(rng_0).unwrap_or(&'0'));
    }
    let rng_1 = &mut rand::thread_rng();
    let mut selected_symbols = String::new();
    for _ in 0..nb_symbols {
        selected_symbols.push(*symbols_choice.choose(rng_1).unwrap_or(&'-'));
    }
    let mut affix_str = String::new();
    match affix {
        AffixType::Prefix => {
            affix_str.push_str(&selected_digits);
            affix_str.push_str(&selected_symbols);
        }
        AffixType::Suffix => {
            affix_str.push_str(&selected_symbols);
            affix_str.push_str(&selected_digits);
        }
    }
    affix_str
}

/// Returns a `String` using the parameters provided
/// # Example
/// ```
/// let nb_symbols_first = 1;
/// let symbols_choice = vec!['-'];
/// assert_eq!(generate_separator(nb_symbols_first, &symbols_choice), "-");
/// let nb_symbols_second = 2;
/// assert_eq!(generate_separator(nb_symbols_second, &symbols_choice), "--");
/// ```
fn generate_separator(nb_symbols: u32, symbols_choice: &Vec<char>) -> String {
    let rng = &mut rand::thread_rng();
    let mut sep = String::new();
    for _ in 0..nb_symbols {
        sep.push(*symbols_choice.choose(rng).unwrap_or(&'_'));
    }
    sep
}

/// `Passphrase` datatype
#[derive(Debug, Default)]
struct Passphrase {
    words: Vec<String>,
    prefix: String,
    suffix: String,
    separator: String,
}

impl fmt::Display for Passphrase {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut string = self.prefix.clone();
        let mut it = self.words.iter().peekable();
        while let Some(word) = it.next() {
            string.push_str(word);
            if it.peek().is_some() {
                string.push_str(&self.separator);
            }
        }
        string.push_str(&self.suffix);
        write!(f, "{string}")
    }
}

impl Passphrase {
    /// Return a `Passphrase` with the given parameters
    fn new(words: &[String], prefix: &str, suffix: &str, separator: &str) -> Self {
        Self {
            words: words.to_owned(),
            prefix: prefix.to_owned(),
            suffix: suffix.to_owned(),
            separator: separator.to_owned(),
        }
    }
}

/// Returns a valid word that fits within the parameters
fn select_word(
    settings: &PassphraseSettings,
    current_nb_words: u32,
    current_length: u32,
) -> &String {
    let rng = &mut rand::thread_rng();
    let mut current_min = settings.min_word_length;
    let mut current_max = settings.max_word_length;
    while current_length + (settings.nb_words - current_nb_words) * current_min
        < settings.min_pass_length
    {
        current_min += 1;
    }
    while current_length + (settings.nb_words - current_nb_words) * current_max
        > settings.max_pass_length
    {
        current_max -= 1;
    }
    let mut selected_word = settings.wordlist.choose(rng).unwrap_or_else(|| {
        eprintln!("Couldn't find word");
        process::exit(2);
    });
    let mut nb_tries = 0;
    while <usize as TryInto<u32>>::try_into(selected_word.len()).unwrap_or(current_min)
        < current_min
        || <usize as TryInto<u32>>::try_into(selected_word.len()).unwrap_or(current_max)
            > current_max
    {
        nb_tries += 1;
        let max_nb_tries: u16 = u16::MAX;
        if nb_tries >= max_nb_tries {
            eprintln!("Tried {max_nb_tries} times but couldn't find words to fit the parameters.");
            process::exit(4);
        }
        selected_word = settings.wordlist.choose(rng).unwrap_or_else(|| {
            eprintln!("Couldn't find a word.");
            process::exit(2);
        });
    }
    selected_word
}

/// Returns a `Passphrase` with the given `PassphraseSettings`
fn generate_passphrase(settings: &PassphraseSettings) -> Passphrase {
    let rng = &mut rand::thread_rng();
    let prefix = settings.prefix.as_ref().map_or_else(
        || {
            generate_affix(
                settings.nb_digits,
                settings.nb_symbols,
                &settings.digits_choice,
                &settings.symbols_choice,
                &AffixType::Prefix,
            )
        },
        std::string::ToString::to_string,
    );
    let suffix = settings.suffix.as_ref().map_or_else(
        || {
            generate_affix(
                settings.nb_digits,
                settings.nb_symbols,
                &settings.digits_choice,
                &settings.symbols_choice,
                &AffixType::Suffix,
            )
        },
        std::string::ToString::to_string,
    );
    let separator = settings.separator.as_ref().map_or_else(
        || generate_separator(settings.nb_separator, &settings.symbols_choice),
        std::string::ToString::to_string,
    );
    let mut current_length: u32 = <usize as TryInto<u32>>::try_into(prefix.len())
        .unwrap_or(settings.nb_digits + settings.nb_symbols)
        + <usize as TryInto<u32>>::try_into(suffix.len())
            .unwrap_or(settings.nb_digits + settings.nb_symbols)
        + (<usize as TryInto<u32>>::try_into(separator.len()).unwrap_or(settings.nb_separator)
            * (settings.nb_words - 1));

    if current_length + (settings.wordlist_largest_length * settings.nb_words)
        < settings.min_pass_length
    {
        eprintln!("Cannot satisfy minimum pass length parameter");
        process::exit(5);
    }

    if current_length + (settings.wordlist_smallest_length * settings.nb_words)
        > settings.max_pass_length
    {
        eprintln!("Cannot satisfy maximum pass length parameter");
        process::exit(6);
    }
    let mut words: Vec<&str> = vec![];
    for _ in 0..settings.nb_words {
        let selected_word = select_word(
            settings,
            <usize as TryInto<u32>>::try_into(words.len()).unwrap_or(settings.min_word_length),
            current_length,
        );
        current_length += <usize as TryInto<u32>>::try_into(selected_word.len())
            .unwrap_or(settings.max_word_length);
        words.push(selected_word);
    }
    words.shuffle(rng);

    let case: CaseType = settings
        .case
        .as_ref()
        .map_or_else(rand::random, std::clone::Clone::clone);
    let mut cased_words: Vec<String> = vec![];

    let mut upper: bool = rand::random();
    for word in words {
        match case {
            CaseType::AlternatingWords => {
                if upper {
                    cased_words.push(CaseType::Upper.convert(word));
                    upper = false;
                } else {
                    cased_words.push(CaseType::Lower.convert(word));
                    upper = true;
                }
            }
            _ => cased_words.push(case.convert(word)),
        }
    }

    Passphrase::new(&cased_words, &prefix, &suffix, &separator)
}

fn main() {
    // get the parameters
    let mut cli = MyCLI::parse();
    if let Some(path) = &cli.read_config {
        let file_as_string = std::fs::read_to_string(path).unwrap_or_default();
        let toml_cli: MyCLI = toml::from_str(&file_as_string).unwrap_or_default();
        cli = cli.from(toml_cli);
    }

    let settings = PassphraseSettings::new(&cli);
    let nb_pass = cli.nb_pass.unwrap_or(1);
    let params = ProgramParam {
        cli: &cli,
        settings: &settings,
    };
    println!("Parameters used:\n{params}");

    // save parameters to a file if asked
    if let Some(path) = &cli.save_config {
        if let Err(error_msg) = std::fs::write(path, toml::to_string(&cli).unwrap_or_default()) {
            eprintln!("Couldn't write the config file ({path}): {error_msg}");
        }
    }

    // prints passphrase(s)
    match nb_pass {
        1 => println!("Generated the passphrase:\n"),
        _ => println!("Generated the {nb_pass} passphrases:\n"),
    }

    let mut passphrases_str = String::new();
    for i in 0..nb_pass {
        let pp = generate_passphrase(&settings);
        let pad = nb_pass.to_string().len();
        println!(
            "Passphrase no. {:0pad$} (length {}):\t{}",
            i + 1,
            pp.to_string().len(),
            pp
        );
        if cli.output.is_some() {
            passphrases_str.push_str(&format!("{pp}\n"));
        };
    }

    // outputs the passphrases to a file if asked
    if let Some(path) = &cli.output {
        if let Err(error_msg) = std::fs::write(path, passphrases_str) {
            eprintln!("Couldn't write the output file ({path}): {error_msg}");
        }
    }
}

#[cfg(test)]
mod unit_tests_main {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_passphrase_to_string() {
        assert_eq!(
            Passphrase {
                words: vec![String::from("first")],
                prefix: String::from("pre"),
                suffix: String::from("suf"),
                separator: String::from("-")
            }
            .to_string(),
            "prefirstsuf"
        );
        assert_eq!(
            Passphrase {
                words: vec![String::from("first"), String::from("second")],
                prefix: String::from("pre"),
                suffix: String::from("suf"),
                separator: String::from("-")
            }
            .to_string(),
            "prefirst-secondsuf"
        );
    }

    #[test]
    fn test_passphrase_new() {
        assert_eq!(
            Passphrase::new(
                &[String::from("first")],
                &String::from("pre"),
                &String::from("suf"),
                &String::from("-")
            )
            .to_string(),
            "prefirstsuf"
        );
        assert_eq!(
            Passphrase::new(
                &[String::from("first"), String::from("second")],
                &String::from("pre"),
                &String::from("suf"),
                &String::from("-")
            )
            .to_string(),
            "prefirst-secondsuf"
        );
    }

    #[test]
    fn test_case_converter() {
        assert_eq!("hello", CaseType::Lower.convert("HeLLo"));
        assert_eq!("HELLO", CaseType::Upper.convert("HeLLo"));
        assert_eq!("Hello", CaseType::Capitalized.convert("HeLLo"));
        let alter = CaseType::Alternating.convert("Hello");
        if alter == "HeLlO" {
            assert_eq!("HeLlO", alter);
        } else {
            assert_eq!("hElLo", alter);
        }
        assert_eq!("Hello", CaseType::Capitalized.convert("HeLLo"));
    }

    #[test]
    fn test_generate_affix() {
        assert_eq!(
            generate_affix(1, 1, &vec!['1'], &vec!['-'], &AffixType::Prefix),
            "1-"
        );
        assert_eq!(
            generate_affix(1, 1, &vec!['1'], &vec!['-'], &AffixType::Suffix),
            "-1"
        );
        assert_eq!(
            generate_affix(2, 2, &vec!['1'], &vec!['-'], &AffixType::Prefix),
            "11--"
        );
        assert_eq!(
            generate_affix(2, 2, &vec!['1'], &vec!['-'], &AffixType::Suffix),
            "--11"
        );
        let digits = "0123456789";
        let symbols = "/*-+.=!_";
        let affix_1 = generate_affix(
            2,
            2,
            &digits.chars().collect(),
            &symbols.chars().collect(),
            &AffixType::Prefix,
        );
        let affix_2 = generate_affix(
            2,
            2,
            &digits.chars().collect(),
            &symbols.chars().collect(),
            &AffixType::Prefix,
        );
        assert_ne!(affix_1, affix_2);
    }

    #[test]
    fn test_generate_separator() {
        assert_eq!(generate_separator(1, &vec!['-']), "-");
        assert_eq!(generate_separator(2, &vec!['-']), "--");
        let symbols = "/*-+.=!_";
        let sep_1 = generate_separator(2, &symbols.chars().collect());
        let sep_2 = generate_separator(2, &symbols.chars().collect());
        assert_ne!(sep_1, sep_2);
    }
}
